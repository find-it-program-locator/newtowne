# Newtowne

A theme for the Find It Program locator project, specifically for Find It Cambridge (Newtowne was the initial settler name for Cambridge, Massachusetts; the likely Wampanoag Native American name.

It may eventually form the basis of all Find It platform sites, probably powered by the [Skins](https://www.drupal.org/project/skins) module to avoid further pitfalls in subtheming.

It is a subtheme of Drutopia's Octavia theme, which is a subtheme of the base theme Bulma for Drupal.  It has Bulma CSS framework, Sass, and Font Awesome built in.


## Browser Support

Autoprefixer & Babel is set to support:

* IE >= 9
* Last 3 versions of modern browsers.

These can be updated at any time within the `package.json`.

In the past year, the traffic of Internet Explorer as identified by Google Analytics has been:

  1. IE 11.0 - 2,423 users, 49% of IE users, with a 45.6% bounce rate.
  2. IE 9.0	 - 2,278 users, 46% of IE users, with a 99.3% bounce rate.
  3. IE 8.0	 - 177 users, 3.6% of IE users, with a 99.4% bounce rate.
  4. IE 6.0  - 17 users, 0.3% of IE users, with a 100% bounce rate.
  5. IE 7.0  - 17 users, 0.3% of IE users, with a 55.6% bounce rate.
  6. IE 10.0 - 15 users, 0.3% of IE users, with a 44.4% bounce rate.

In conclusion, targeting IE 9 and above makes sense.  The high bounce rates suggest the users being identified as those browsers are not human or are not ultimately interested in the content of the site— or that the site is technically broken for them.  For the nearly half of IE users still coming with IE 9 (only 2% of the total visitors) the bounce rate is a concern so the new site will try to improve on that.

## Getting Started

### Setting up and using style building tools

If you haven't yet, [install nvm](https://github.com/creationix/nvm).

Run the following commands from the theme directory

#### Use the right version of node with:

`nvm use`

_This command will look at your `.nvmrc` file and use the version node.js specified in it. This ensures all developers use the same version of node for consistency._

#### If that version of node isn't installed, install it with:

`nvm install`

#### Install npm dependencies with

`npm install`

_This command looks at `package.json` and installs all the npm dependencies specified in it.  Some of the dependencies include gulp, autoprefixer, gulp-sass and others._

#### Compile Sass to CSS

`./css`
