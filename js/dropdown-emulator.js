function showDropDown(element){
  var listElement = element.parentNode.getElementsByTagName('ul').item(0);
  if(listElement.getAttribute('style')==="display:block;"){
    listElement.setAttribute('style','display:none;');
  }else{
    listElement.setAttribute('style','display:block;');
  }
}

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.dropdownEmulator = {
    attach: function (context) {
      $(".dropdown-emulator-select-list").hide();
    }
  };

})(jQuery, Drupal);
